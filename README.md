# oc sso-login

An `oc` / `kubectl` plugin to faciliate login to OKD4 clusters in CERN computing environment.

It will:
1. retrieve the cluster and authentication configuration from `_config.${CLUSTER_NAME}.okd.cern.ch`
2. fetch a valid SSO token from Keycloak using the "Device Authorization Grant" flow (with 2FA support)
3. perform a [token exchange](https://auth.docs.cern.ch/user-documentation/oidc/exchange-for-api/) to the application registration of the appropriate cluster
4. use the exchanged token to authenticate against the Openshift API with the help of our [okd-api-sso-token-exchange](https://gitlab.cern.ch/paas-tools/okd4-deployment/okd-api-sso-token-exchange)
5. update the local kubeconfig context with the new credentials

Note that the application registration used in step 2 must be configured as a [public client](https://auth.docs.cern.ch/user-documentation/oidc/token-requests/), since it cannot store  secure credentials.

Visualization:

![Visualization of oc-sso-login flow](./oc-sso-login-flow.svg)

---

Non-functional requirements:
* Target platform: Python 3.6 (LXPLUS8 / AIADM as of May 2022).
* Low-dependency (ideally none) and single file - this makes it easier to distribute the plugin to users.

References:
* https://docs.openshift.com/container-platform/4.9/cli_reference/openshift_cli/extending-cli-plugins.html
* https://gitlab.cern.ch/paas-tools/okd4-deployment/okd-api-sso-token-exchange/-/blob/master/main.go
* https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie/-/blob/master/auth_get_sso_cookie/cern_sso.py

## Installation

To install this plugin, simply drop the `oc-sso-login.py` somewhere in your `$PATH` (e.g. `/usr/local/bin` or
`~/.local/bin/`), rename it to `oc-sso_login` and make sure it's executable.
Then you can invoke it with `oc sso-login -h`.

```sh
pip install -r https://gitlab.cern.ch/paas-tools/oc-sso-login/-/raw/master/requirements.txt
curl -sL https://gitlab.cern.ch/paas-tools/oc-sso-login/-/raw/master/oc-sso-login.py -o ~/.local/bin/oc-sso_login
chmod +x ~/.local/bin/oc-sso_login
oc sso-login --version
> oc-sso_login (version 0.2.1)
```

## Testing

Type-checking is far from a silver-bullet, yet it helps to validate the basic function calls etc.:

```sh
$ pip install -r requirements-dev.txt
$ pylint oc-sso-login.py
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ mypy oc-sso-login.py
Success: no issues found in 1 source file
```

## Set up test application registration

Go to the [Application Portal](https://application-portal.web.cern.ch/) and click on "Add an application" (preferably replace `my` with your username):

1. Application identifier: `my-okd4-sso-login`
2. Name: `my-okd4-sso-login`
3. Description: `Helper application for OKD4 cluster login`
4. Resource category: `Test`

Add a new SSO registration in the dedicated column:

1. Protocol: `OIDC`
2. Redirect URIs: `http://localhost`
3. Tick the box `My application cannot store a client secret safely`
4. "Submit"

Request token exchange permissions by clicking on the blue lock icon next to the SSO registration:
1. Under "Allowed Permissions", click on "Request token exchange permission"
2. Enter the client ID associated to the OKD4 cluster you want to connect to (e.g. `paas-stg_id`)
3. Click on the green button to add it.

**Note**: in case you are an administrator of the cluster application registration (e.g. `paas-stg_id`), the permission will automatically be granted.
Otherwise, an admin of the application will need to approve the request first!

## DNS cluster configuration

The cluster and authentication configuration is read from DNS TXT records located at `_config.${CLUSTER_NAME}.okd.cern.ch`.
For development purposes, the values can be injected manually as follows (assuming you are using a custom login application as described above):

```sh
CLUSTER_NAME=clu-jack-dev
LOGIN_APPLICATION=my-okd4-sso-login
AUDIENCE=clu-jack-dev_id
AUTH_URL=https://auth.cern.ch/auth/realms/cern

cat - <<EOF
server 137.138.28.176
zone okd.cern.ch
update delete _config.${CLUSTER_NAME}.okd.cern.ch 3600 TXT
update add _config.${CLUSTER_NAME}.okd.cern.ch 3600 TXT "api_url=https://api.${CLUSTER_NAME}.okd.cern.ch"
update add _config.${CLUSTER_NAME}.okd.cern.ch 3600 TXT "token_exchange_url=https://token-exchange.${CLUSTER_NAME}.cern.ch"
update add _config.${CLUSTER_NAME}.okd.cern.ch 3600 TXT "audience_id=${AUDIENCE}"
update add _config.${CLUSTER_NAME}.okd.cern.ch 3600 TXT "login_application_id=${LOGIN_APPLICATION}"
update add _config.${CLUSTER_NAME}.okd.cern.ch 3600 TXT "auth_url=${AUTH_URL}"
show
send
quit
EOF | nsupdate -y ${TSIG_KEY_NAME_INTERNAL}:${TSIG_KEY_DATA_INTERNAL}
```

For dev clusters, `oc-sso-login` needs to be called with the `--insecure-tls-skip-verify` option!

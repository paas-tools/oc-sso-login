#!/usr/bin/env python3
"""
A kubectl plugin to faciliate login to OKD4 clusters in CERN environment.
It will fetch a valid SSO token from Keycloak (with 2FA support)
and use that to authenticate against the Openshift API.
Afterwards, the kubeconfig is updated with the new context.
"""

__author__ = "Jack Henschel"
__version__ = "0.4.1"
__license__ = "MIT"

# Python stdlib
from datetime import datetime
import argparse
import base64
import functools
import json
import os
import subprocess
import sys
import time

# third-party libraries
import requests
import dns.resolver

# Global shared variables which are only set at application startup
LOG_LEVEL: int = 0
APP_NAME: str = ""
HTTP_CLIENT: requests.Session = requests.Session()

#####################
# Helper functions  #
#####################

# Note: Python's native "str.removeprefix" is only supported in Python >= 3.9
# Unfortunately, lxplus8 and aiadm still use Python 3.6
# https://docs.python.org/3.9/library/stdtypes.html?highlight=removeprefix#str.removeprefix
def remove_prefix(s, prefix):
    if s.startswith(prefix):
        return s[len(prefix):]
    return s

def remove_suffix(s, suffix):
    if s.endswith(suffix):
        return s[:-(len(suffix))]
    return s

def print_stderr(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def print_verbose(*args, **kwargs):
    if LOG_LEVEL >= 1:
        print_stderr(*args, **kwargs)

#####################################################
# Functions for interacting with Openshift clusters #
#####################################################

def exchange_openshift_token(access_token: str, token_exchange_url: str) -> str:
    params: dict = {'redirect-uri': 'http://localhost'} # dummy value, required by the API
    url: str = f"{token_exchange_url}/openshift-api-token"
    response = HTTP_CLIENT.get(url, headers={'Authorization': f"Bearer {access_token}"}, params=params)
    if not response.ok:
        print_stderr(response.headers)
        print_stderr(response.text)
        raise Exception(f"Endpoint '{response.url}' returned unexpected status code '{response.status_code}'")

    data: dict = response.json()
    return data['token']


class ClusterConfig: # pylint: disable=too-few-public-methods
    """
    An object of the ClusterConfig class represents the connection and login configuration
    of an OKD4 cluster.
    The object needs to be instatiated with the short cluster name (e.g. "paas") and then the
    lookup() method can be called. This method will retrieve the cluster configuration from
    the DNS TXT entries at _config.{cluster_name}.okd.cern.ch
    Afterwards, the object properties listed below can be used externally.
    """

    name: str
    api_url: str
    token_exchange_url: str
    audience_id: str
    login_application_id: str
    auth_url: str

    def __init__(self, name: str):
        self.name: str = name

    def lookup(self):
        domain: str = f"_config.{self.name}.okd.cern.ch."
        print_verbose(f"Looking up configuration for cluster '{self.name}' at '{domain}'")

        # https://dnspython.readthedocs.io/en/latest/resolver-class.html#dns.resolver.Answer
        # Note: dnspython v2 has deprecated the "query()" method, but dnspyton v1.15 (lxplus8) doesn't support "resolve()"!
        answer: dns.resolver.Answer
        if 'resolve' in dir(dns.resolver): # dnspython v2.x
            answer = dns.resolver.resolve(domain, "TXT")
        else: # dnspython v1.x
            answer = dns.resolver.query(domain, "TXT")

        if not answer.rrset:
            raise Exception(f"DNS TXT lookup for {domain} returned no results")

        for record in answer.rrset:
            print_verbose(f"Got TXT record: {record}")

            r: str = record.to_text().strip('"')
            if r.startswith("api_url="):
                self.api_url = remove_prefix(r, "api_url=").rstrip('/')
                print_verbose(f"API URL: {self.api_url}")

            elif r.startswith("token_exchange_url="):
                self.token_exchange_url = remove_prefix(r, "token_exchange_url=").rstrip('/')
                print_verbose(f"Token Exchange URL: {self.token_exchange_url}")

            elif r.startswith("audience_id="):
                self.audience_id = remove_prefix(r, "audience_id=")
                print_verbose(f"Audience ID: {self.audience_id}")

            elif r.startswith("login_application_id="):
                self.login_application_id = remove_prefix(r, "login_application_id=")
                print_verbose(f"Login Application ID: {self.login_application_id}")

            elif r.startswith("auth_url="):
                self.auth_url = remove_prefix(r, "auth_url=").rstrip('/')
                print_verbose(f"Authentication issuer URL: {self.auth_url}")

        # perform validation
        # this checks that we have all required configuration items and they are somewhat plausible
        assert self.api_url.startswith("https://"), "API URL invalid: does not start with https://"
        assert self.api_url.endswith(".cern.ch"), "API URL invalid: does not end with cern.ch"
        assert self.token_exchange_url.startswith("https://"), "Token Exchange URL invalid: does not start with https://"
        assert self.token_exchange_url.endswith(".cern.ch"), "Token Exchange URL invalid: does not end with cern.ch"
        assert len(self.audience_id) > 0, "Invalid audience ID: empty"
        assert len(self.login_application_id) > 0, "Invalid login application ID: empty"
        assert self.auth_url.startswith("https://"), "Authentication server URL invalid: does not start with https://"

###########################################
# Functions for interacting with CERN SSO #
###########################################

def get_valid_access_token(cluster_config: ClusterConfig) -> str:
    """
    get_valid_access_token implements the authentication workflow and logic with CERN SSO.
    No matter what the local state is (in terms of cached access tokens),
    at the end it always returns a valid, fresh access token for the desired audience.
    """

    print_verbose(f"get_valid_access_token: application_id '{cluster_config.login_application_id}'" \
                  f" audience '{cluster_config.audience_id}'")
    # Check if we already have a valid token locally. If so, return that.
    audience_token_file: str = token_file_path(cluster_config.audience_id)
    audience_token: dict = load_token(audience_token_file)
    if audience_token:
        if is_valid_token(audience_token['access_token']):
            print_verbose(f"Found valid token for audience '{cluster_config.audience_id}' in '{audience_token_file}'")
            return audience_token['access_token']

    # In case we don't have a valid token, check if we have a valid token for the generic login application
    application_token_file: str = token_file_path(cluster_config.login_application_id)
    application_token: dict = load_token(application_token_file)
    if application_token:
        # Check if access token is (still) valid
        if is_valid_token(application_token['access_token']):
            # If we have a valid token for the login application, use that to do a token exchange flow for the target audience
            print_verbose(f"Found valid token for audience '{cluster_config.login_application_id}'" \
                          f" in '{application_token_file}' - using it for token exchange")
        else:
            # Access token is no longer valid. Try to renew it, if possible
            try:
                print_verbose(f"Attempting token refresh for audience {cluster_config.login_application_id} - this may not work")
                application_token = token_refresh(application_token['refresh_token'], cluster_config.auth_url, cluster_config.login_application_id)
                print_verbose(f"Successfully refreshed token for audience '{cluster_config.login_application_id}'")
                store_token(application_token_file, application_token)
            except Exception as ex: # pylint: disable=broad-except
                application_token = {}
                print_verbose(ex)

    # Use the login application token to perform a token exchange to the target application
    if application_token:
        audience_token = public_token_exchange(cluster_config.login_application_id,
                                               cluster_config.audience_id,
                                               application_token['access_token'],
                                               cluster_config.auth_url,
                                               )
        print_verbose(f"Successfully exchanged token for audience '{cluster_config.audience_id}', saving token locally.")
        store_token(audience_token_file, audience_token)
        return audience_token['access_token']

    # We don't have any valid tokens, start the flow from the beginning with 2FA login
    application_token = device_authorization_login(cluster_config.login_application_id,
                                                   cluster_config.auth_url,
                                                   )
    print_verbose(f"Successfully obtained SSO token for '{cluster_config.login_application_id}'," \
                  f" saving token locally.")
    store_token(application_token_file, application_token)

    print_verbose(f"Performing token exchange from '{cluster_config.login_application_id}'" \
                  f"to '{cluster_config.audience_id}'")
    audience_token = public_token_exchange(cluster_config.login_application_id,
                                           cluster_config.audience_id,
                                           application_token['access_token'],
                                           cluster_config.auth_url,
                                           )
    print_verbose(f"Successfully exchanged token for audience '{cluster_config.audience_id}'," \
                  f"saving token locally.")
    store_token(audience_token_file, audience_token)
    return audience_token['access_token']


def token_refresh(refresh_token: str, issuer_url: str, client_id: str) -> dict:
    """
    Performs a Token Refresh with the Keycloak server specified by 'issuer_url' for the 'client_id' application.
    If successful, returns the full token response as a dictionary.
    """
    r = HTTP_CLIENT.post(f"{issuer_url}/protocol/openid-connect/token",
                      data={
                          "client_id": client_id,
                          "grant_type": "refresh_token",
                          "refresh_token": refresh_token,
                      },
        )

    if not r.ok:
        raise Exception(
            f"Authentication request failed: token refresh was not successful: {r.text}")

    new_token: dict = r.json()
    print_verbose("Token refresh response:", new_token)
    return new_token


def device_authorization_login(client_id: str, issuer_url: str) -> dict:
    """
    Performs a Device Authorization Login Flow with the Keycloak server specified by 'issuer_url'
    for the application 'client_id'.
    Returns the full token response as a dictionary.

    This code is adapted from CERN's auth_get_sso_ccokie/cern_sso.py, to avoid a dependency on this
    (closed-source) library.
    """
    r = HTTP_CLIENT.post(f"{issuer_url}/protocol/openid-connect/auth/device",
                      data={
                          "client_id": client_id,
                      },
        )

    if not r.ok:
        raise Exception(
            f"Authentication request failed: Device authorization response was not successful: {r.text}")

    auth_response = r.json()

    print_verbose("Authentication request response:", auth_response)

    print(f"Open the following link to log in:\n  {auth_response['verification_uri_complete']}\nWaiting for login...")

    signed_in = False

    while not signed_in:
        time.sleep(auth_response.get('interval', 10))
        r_token = HTTP_CLIENT.post(
            f"{issuer_url}/protocol/openid-connect/token",
            data={
                "client_id": client_id,
                "grant_type": "urn:ietf:params:oauth:grant-type:device_code",
                "device_code": auth_response['device_code'],
            },
        )
        print_verbose(f"The token response was: {r_token.json()}")
        signed_in = r_token.ok

    token_response: dict = r_token.json()
    return token_response

def public_token_exchange(client_id: str, audience: str, token: str, issuer_url: str) -> dict:
    """
    Performs a Token Exchange with the Keycloak server specified by 'issuer_url'
    from the initial application 'client_id' to the target application 'audience'.
    Returns the full token response as a dictionary.

    This code is adapted from CERN's auth_get_sso_ccokie/cern_sso.py, to avoid a dependency on this
    (closed-source) library.
    """
    r_token = HTTP_CLIENT.post(
        f"{issuer_url}/protocol/openid-connect/token",
        data={
            "client_id": client_id,
            "audience": audience,
            "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange",
            "requested_token_type": "urn:ietf:params:oauth:token-type:refresh_token",
            "subject_token": token
        },
    )
    if not r_token.ok:
        print(f"SSO token exchange response was not successful: {r_token.json()}")
        r_token.raise_for_status()
    token_response: dict = r_token.json()
    return token_response


def is_valid_token(token: str) -> bool:
    try:
        decoded: dict = decode_jwt_without_verification(token)
        expiry_time: int = decoded['exp']
    except Exception: # pylint: disable=broad-except
        return False

    return expiry_time > datetime.now().timestamp()

def decode_jwt_without_verification(token: str) -> dict:
    """
    This function decodes a JWT token and returns its payload WITHOUT verifying any signatures or headers.
    The implementation is based on the PyJWT jws._load() method:
    https://github.com/jpadilla/pyjwt/blob/8215bf8fd64db41fd7f4972fba905c4274a3e81c/jwt/api_jws.py#L212
    This avoids a dependency on said library.
    """
    b: bytes = token.encode("utf-8")
    signing_input, crypto_segment = b.rsplit(b".", 1)
    # we are not interested in the crypto_segment since this function does not verify the signature
    _ = crypto_segment
    header_segment, payload_segment = signing_input.rsplit(b".", 1)
    _ = header_segment

    # add padding if necessary
    rem: int = len(payload_segment) % 4
    if rem > 0:
        payload_segment += b"=" * (4 - rem)

    # decode bytes payload into a dictionary
    payload: dict = json.loads(base64.urlsafe_b64decode(payload_segment))

    return payload


########################################################
# Functions for interacting with the local environment #
########################################################

def update_kubeconfig_context(openshift_token: str, cluster_name: str, cluster_api_url: str, skip_tls_verify: bool) -> None:
    binary_name: str = APP_NAME.split('-')[0]
    if binary_name == "oc":
        cmd = [
            "oc",
            "login",
            f"--server={cluster_api_url}",
            f"--token={openshift_token}",
            f"--insecure-skip-tls-verify={str(skip_tls_verify).lower()}",
        ]
        res = subprocess.run(cmd, encoding='utf-8', stderr=subprocess.STDOUT, check=False)

        if res.returncode != 0:
            print(cmd)
            print(res.stdout, res.stderr)
            raise Exception(f"oc login returned exit code {res.returncode}")

        print(f"Successfully logged in to '{cluster_name}'")
        # TODO: rename the auto-created context name into a "friendly" name
    else:
        # TODO: in prinicpal, this function could also support kubectl
        raise Exception(f"Unsupported binary '{binary_name}'")

def load_token(token_file: str) -> dict:
    """
    This function loads the contents of the JSON file $token_file.
    Returns empty if the file is not found or does not contain valid JSON data.
    """
    # check if file present
    if not os.path.isfile(token_file):
        return {}

    # load file
    with open(token_file, 'r', encoding='utf-8') as f:
        try:
            data = json.load(f)
        except json.decoder.JSONDecodeError:
            return {}

    return data

def token_file_path(audience: str) -> str:
    xdg_config_home: str = os.environ.get('XDG_CONFIG_HOME') \
        or os.path.join(os.path.expanduser('~'), '.config')
    token_file: str = os.path.join(xdg_config_home, APP_NAME, audience + '_token.json')
    return token_file

def store_token(path: str, data: dict) -> None:
    print_verbose(f"Storing token data in '{path}'")
    os.makedirs(os.path.dirname(path), exist_ok=True)
    # open file in write+truncate mode (erasing any previous data)
    with open(path, 'w+', encoding='utf-8') as f:
        os.chmod(path, 0o600) # only owner can read and write
        json.dump(data, f)

def main(args) -> None:
    """ Main logic of the CLI plugin """

    cluster_name: str
    if args.server:
        # assuming layout like: https://api.${CLUSTER_NAME}.okd.cern.ch
        cluster_name = args.server.split('.', 2)[1]
    else:
        cluster_name = args.cluster

    if not cluster_name:
        print_stderr(f"No cluster name or URL supplied, please see --help")
        sys.exit(1)

    print_verbose(f"Assuming cluster name '{cluster_name}'")

    # First: figure out the connection details of the cluster we want to connect to
    cluster_config = ClusterConfig(cluster_name)
    try:
        cluster_config.lookup()
    except Exception as ex: # pylint: disable=broad-except
        print_stderr(f"Failed to lookup cluster configuration for '{cluster_name}': {ex.__class__.__name__} {ex}")
        sys.exit(1)

    # Next: obtain a valid SSO token
    access_token: str = get_valid_access_token(cluster_config)

    # Then: use this token to authenticate with the Openshift API
    openshift_token: str = exchange_openshift_token(access_token, cluster_config.token_exchange_url)

    # Finally: update the local environment (kubeconfig context) with the new credentials
    update_kubeconfig_context(openshift_token, cluster_config.name, cluster_config.api_url, args.insecure_skip_tls_verify)

# This is executed when run from the command line
if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("cluster", action="store", nargs="?",
                        help="Friendly name of a CERN OKD4 cluster (e.g. `paas`)")
    parser.add_argument("--token", action="store",
                        help="Openshift / Kubernetes API token (should start with `sha256~`)")
    parser.add_argument("--server", action="store",
                        help="Full address of the Openshift / Kubernetes API server, e.g. `https://api.paas.okd.cern.ch`")
    parser.add_argument("--insecure-skip-tls-verify", action="store_true", default=False,
                        help="Disable TLS certificate verfication for HTTP calls")

    parser.add_argument("-v", "--verbose", action="count", default=0,
                        help="Enable verbose log messages")

    # Specify output of "--version"
    parser.add_argument("--version", action="version",
                        version=f"%(prog)s (version {__version__})",
                        help="Print script version number")

    # Parse CLI arguments
    arguments = parser.parse_args()

    # Setup global variables
    APP_NAME = parser.prog
    LOG_LEVEL = arguments.verbose

    # Setup shared HTTP session object
    # https://requests.readthedocs.io/en/latest/user/advanced/#session-objects
    # prepend tool version to user-agent header (default is "python-requests/2.x.x")
    HTTP_CLIENT.headers.update({
        'User-Agent': f"oc-sso-login {__version__}; {requests.utils.default_user_agent()}",
    })
    HTTP_CLIENT.max_redirects = 0
    HTTP_CLIENT.verify = not arguments.insecure_skip_tls_verify
    # ignore .netrc file and other environment variables (caused INC3204565):
    # https://github.com/psf/requests/issues/3929
    HTTP_CLIENT.trust_env = False
    # set global timeout for all requests:
    HTTP_CLIENT.request = functools.partial(HTTP_CLIENT.request, timeout=15) # type: ignore

    # Call main
    main(arguments)
